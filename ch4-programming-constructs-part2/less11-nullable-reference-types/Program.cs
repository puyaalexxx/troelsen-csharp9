﻿using System;

#nullable enable

namespace less11_nullable_reference_types
{
    class Program
    {
        static void Main(string[] args)
        {
            string? nullableString = null;
            TestClass? myNullableClass = null;
            
            //omitted for brevity
            Console.WriteLine("***** Fun with Nullable Data *****\n");
            DatabaseReader dr = new DatabaseReader();
            
            //null coelsing operator
            
            // If the value from GetIntFromDatabase() is null,
            // assign local variable to 100.
            int myData = dr.GetIntFromDatabase() ?? 100;
            Console.WriteLine("Value of myData: {0}", myData);
            
            //Null-coalescing assignment operator
            int? nullableInt = null;
            nullableInt ??= 12;
            nullableInt ??= 14;
            Console.WriteLine(nullableInt);
            
            Console.ReadLine();
        }
        
        static void TesterMethod(string[] args)
        {
            // We should check for null before accessing the array data!
            if (args != null)
            {
                Console.WriteLine($"You sent me {args.Length} arguments.");
            }
        }
        
        //using null conditional operator - elvis
        static void TesterMethod2(string[] args)
        {
            // We should check for null before accessing the array data!
            Console.WriteLine($"You sent me {args?.Length} arguments.");
        }
    }
    
    public class TestClass
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
    
    class DatabaseReader
    {
        // Nullable data field.
        public int? numericValue = null;
        public bool? boolValue = true;
        // Note the nullable return type.
        public int? GetIntFromDatabase()
        { return numericValue; }
        // Note the nullable return type.
        public bool? GetBoolFromDatabase()
        { return boolValue; }
    }
}