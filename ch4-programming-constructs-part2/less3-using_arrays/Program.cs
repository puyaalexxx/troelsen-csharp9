﻿using System;

static void SystemArrayFunctionality()
{
    Console.WriteLine("=> Working with System.Array.");
    // Initialize items at startup.
    string[] gothicBands = {"Tones on Tail", "Bauhaus", "Sisters of Mercy"};
    // Print out names in declared order.
    Console.WriteLine("-> Here is the array:");
    for (int i = 0; i < gothicBands.Length; i++)
    {
        // Print a name.
        Console.Write(gothicBands[i] + ", ");
    }
    Console.WriteLine("\n");

    // Reverse them...
    Array.Reverse(gothicBands);
    Console.WriteLine("-> The reversed array");
    // ... and print them.
    for (int i = 0; i < gothicBands.Length; i++)
    {
        // Print a name.
        Console.Write(gothicBands[i] + ", ");
    }
    Console.WriteLine("\n");
    // Clear out all but the first member.
    Console.WriteLine("-> Cleared out all but one...");
    Array.Clear(gothicBands, 1, 2);
    for (int i = 0; i < gothicBands.Length; i++)
    {
        // Print a name.
        Console.Write(gothicBands[i] + ", ");
    }
    Console.WriteLine();
}

static void Rangesarrays()
{
    string[] gothicBands = {"Tones on Tail", "Bauhaus", "Sisters of Mercy"};
    
    for (int i = 0; i < gothicBands.Length; i++)
    {
        Index idx = i;
        // Print a name
        Console.Write(gothicBands[idx] + ", ");
    }
    
    for (int i = 1; i <= gothicBands.Length; i++)
    {
        Index idx = ^i;
        // Print a name
        Console.Write(gothicBands[idx] + ", ");
    }
    
    foreach (var itm in gothicBands[0..2])
    {
        // Print a name
        Console.Write(itm + ", ");
    }
    Console.WriteLine("\n");
    
    Range r1 = 0..2; //the end of the range is exclusive
    foreach (var itm in gothicBands[r1])
    {
        // Print a name
        Console.Write(itm + ", ");
    }
    Console.WriteLine("\n");
    
    //Ranges can be defined using integers or Index variables. The same result will occur with the following code:
    Index idx1 = 0;
    Index idx2 = 2;
    Range r2 = idx1..idx2; //the end of the range is exclusive
    foreach (var itm in gothicBands[r2])
    {
        // Print a name
        Console.Write(itm + ", ");
    }
    Console.WriteLine("\n");
}