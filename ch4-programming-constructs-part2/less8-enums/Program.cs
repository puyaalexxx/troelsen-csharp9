﻿using System;

namespace less8_enums
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("**** Fun with Enums *****");
            // Make an EmpTypeEnum variable.
            EmpType emp = EmpType.Contractor;
            AskForBonus(emp);
            
            // Print storage for the enum.
            Console.WriteLine("EmpType uses a {0} for storage",
                Enum.GetUnderlyingType(emp.GetType()));
            
            // This time use typeof to extract a Type.
            Console.WriteLine("EmpType uses a {0} for storage",
                Enum.GetUnderlyingType(typeof(EmpTypeEnum)));
            
            // Prints out "emp is a Contractor".
            Console.WriteLine("emp is a {0}.", emp.ToString());
            
            // Prints out "Contractor = 100".
            Console.WriteLine("{0} = {1}", emp.ToString(), (byte)emp);
            
            Console.WriteLine("------------------------------");
            
            // These types are enums in the System namespace.
            EmpType e2 = EmpType.Contractor;
            DayOfWeek day = DayOfWeek.Monday;
            ConsoleColor cc = ConsoleColor.Gray;
            EvaluateEnum(e2);
            EvaluateEnum(day);
            EvaluateEnum(cc);
            
            Console.WriteLine("------------------------------");
            
            //using enum flags 
            ContactPreferenceEnum emailAndPhone = ContactPreferenceEnum.Email | ContactPreferenceEnum.Phone;
            
            Console.WriteLine("None? {0}", (emailAndPhone | ContactPreferenceEnum.None) == emailAndPhone); 
            Console.WriteLine("Email? {0}", (emailAndPhone | ContactPreferenceEnum.Email) == emailAndPhone);
            Console.WriteLine("Phone? {0}", (emailAndPhone | ContactPreferenceEnum.Phone) == emailAndPhone); 
            
            Console.ReadLine();
        }
        
        // Enums as parameters.
        static void AskForBonus(EmpType e)
        {
            switch (e) {
                case EmpType.Manager:
                    Console.WriteLine("How about stock options instead?");
                    break;
                case EmpType.Grunt:
                    Console.WriteLine("You have got to be kidding...");
                    break;
                case EmpType.Contractor:
                    Console.WriteLine("You already get enough cash...");
                    break;
                case EmpType.VicePresident:
                    Console.WriteLine("VERY GOOD, Sir!");
                    break; }
        }
        
        // This method will print out the details of any enum.
        static void EvaluateEnum(System.Enum e)
        {
            Console.WriteLine("=> Information about {0}", e.GetType().Name);
            Console.WriteLine("Underlying storage type: {0}",
                Enum.GetUnderlyingType(e.GetType()));
            // Get all name-value pairs for incoming parameter.
            Array enumData = Enum.GetValues(e.GetType());
            Console.WriteLine("This enum has {0} members.", enumData.Length);
            // Now show the string name and associated value, using the D format
            // flag (see Chapter 3).
            for(int i = 0; i < enumData.Length; i++)
            {
                Console.WriteLine("Name: {0}, Value: {0:D}",
                    enumData.GetValue(i));
            }
        }
    }
    
    // A custom enumeration.
    enum EmpTypeEnum
    {
        Manager,
        Grunt,
        Contractor,
        VicePresident // = 3
    }
    
    // Begin with 102.
    enum EmpTypeEnum2
    {
        Manager = 102,
        Grunt,        // = 103
        Contractor,   // = 104
        VicePresident // = 105
    }
    
    // Elements of an enumeration need not be sequential!
    enum EmpType
    {
        Manager = 10,
        Grunt = 1,
        Contractor = 100,
        VicePresident = 9
    }
    
    enum EmpTypeByteEnum : byte
    {
        Manager = 10,
        Grunt = 1,
        Contractor = 100,
        VicePresident = 9
    }
    
    [Flags]
    public enum ContactPreferenceEnum
    {
        None = 1,
        Email = 2,
        Phone = 4,
        Ponyexpress = 6
    }

}