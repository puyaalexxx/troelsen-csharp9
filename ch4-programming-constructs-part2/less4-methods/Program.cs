﻿using System;
using System.Diagnostics.CodeAnalysis;

//expresion-bodied members
static int Add(int x, int y) => x + y;

static int LocalFunction(int x, int y)
{
    //Do some validation here
    return Add();
    int Add() {
        return x + y;
    } 
}

//C# 9 adding attributes to local functions
#nullable enable
static void Process(string?[] lines, string mark)
{
    foreach (var line in lines)
    {
        if (IsValid(line))
        {
            // Processing logic...
        }
    }
    bool IsValid([NotNullWhen(true)] string? line)
    {
        return !string.IsNullOrEmpty(line) && line.Length >= mark.Length;
    }
}

static int StaticLocalFunction(int x, int y)
{
    //Do some validation here
    return Add(x,y);
    static int Add(int x, int y)
    {
        return x + y;
        
    }
}

