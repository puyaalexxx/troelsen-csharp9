﻿using System;

namespace less2_thisd_keyword
{
    class Program
    {
        static void Main(string[] args)
        {
            // Make a MotorCycle.
            MotorCycle c = new MotorCycle(5);
            c.SetDriverName("Tiny");
            c.PopAWheely();
            Console.WriteLine("Rider name is {0}", c.driverName);


            // driverName = "", driverIntensity = 0
            MotorCycle m1 = new MotorCycle();
            Console.WriteLine("Name= {0}, Intensity= {1}",
            m1.driverName, m1.driverIntensity);
            // driverName = "Tiny", driverIntensity = 0
            MotorCycle m2 = new MotorCycle(name: "Tiny");
            Console.WriteLine("Name= {0}, Intensity= {1}",
            m2.driverName, m2.driverIntensity);
            // driverName = "", driverIntensity = 7
            MotorCycle m3 = new MotorCycle(7);
            Console.WriteLine("Name= {0}, Intensity= {1}",
            m3.driverName, m3.driverIntensity);
        }
    }
}
