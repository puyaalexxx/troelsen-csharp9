﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace less2_thisd_keyword
{
    class MotorCycle
    {
        public int driverIntensity;
        // New members to represent the name of the driver.
        public string driverName;
        public void SetDriverName(string name) => this.driverName = name;

        public void PopAWheely()
        {
            for (int i = 0; i <= driverIntensity; i++)
            {
                Console.WriteLine("Yeeeeeee Haaaaaeewww!");
            }
        }
        // Constructor chaining.
        public MotorCycle() { }
        public MotorCycle(int intensity)
        : this(intensity, "") { }
        public MotorCycle(string name)
        : this(0, name) { }

        // This is the 'master' constructor that does all the real work.
        public MotorCycle(int intensity, string name)
        {
            if (intensity > 10)
            {
                intensity = 10;
            }
            driverIntensity = intensity;
            driverName = name;
        }

        // Single constructor using optional args.
        //public MotorCycle(int intensity = 0, string name = "")
        //{
        //    if (intensity > 10)
        //    {
        //        intensity = 10;
        //    }
        //    driverIntensity = intensity;
        //    driverName = name;
        //}
    }
}
