﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace less7_readonly_data
{
    class MyMathClass
    {
        public const double PI = 3.14;
        public readonly double PI1;
        public static readonly double PI2 = 3.14;
        public MyMathClass()
        {
            PI1 = 3.14;
        }

        static MyMathClass()
        { PI2 = 3.14; }
    }
}
