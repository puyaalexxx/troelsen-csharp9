﻿using System;

namespace less7_readonly_data
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Const *****\n");
            Console.WriteLine("The value of PI is: {0}", MyMathClass.PI);
            // Error! Can't change a constant!
            // MyMathClass.PI = 3.1444;

            Console.WriteLine("***** Static readonly *****");
            Console.WriteLine("The value of PI is: {0}", MyMathClass.PI2);

            Console.ReadLine();
        }
    }
}
