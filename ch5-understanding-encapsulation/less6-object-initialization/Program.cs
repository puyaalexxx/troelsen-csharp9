﻿using System;

namespace less6_object_initialization
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Object Init Syntax *****\n");
            // Make a Point by setting each property manually.
            Point firstPoint = new Point();
            firstPoint.X = 10;
            firstPoint.Y = 10;
            firstPoint.DisplayStats();

            // Or make a Point via a custom constructor.
            Point anotherPoint = new Point(20, 20);
            anotherPoint.DisplayStats();

            // Or make a Point using object init syntax.
            Point finalPoint = new Point { X = 30, Y = 30 };
            finalPoint.DisplayStats();

            ///////////////using init only setters

            //Make readonly point after construction
            PointReadOnlyAfterCreation firstReadonlyPoint = new PointReadOnlyAfterCreation(20, 20);
            firstReadonlyPoint.DisplayStats();
            // Or make a Point using object init syntax.
            PointReadOnlyAfterCreation secondReadonlyPoint = new PointReadOnlyAfterCreation
            {
                X = 30,
                Y = 30
            };
            secondReadonlyPoint.DisplayStats();

            //The next two lines will not compile
            //secondReadonlyPoint.X = 10;
            //secondReadonlyPoint.Y = 10;

            /////////////////////////calling constructores with initialization syntax
            // Here, the default constructor is called implicitly.
            Point finalPoint1 = new Point { X = 30, Y = 30 };
            // Here, the default constructor is called explicitly.
            Point finalPoint2 = new Point() { X = 30, Y = 30 };
            // Calling a custom constructor.
            Point pt = new Point(10, 16) { X = 100, Y = 100 };

            ////////////
            // Calling a more interesting custom constructor with init syntax.
            Point goldPoint = new Point(PointColorEnum.Gold) { X = 90, Y = 20 };
            goldPoint.DisplayStats();

            ////////////////
            ///// Create and initialize a Rectangle.
            Rectangle myRect = new Rectangle
            {
                TopLeft = new Point { X = 10, Y = 10 },
                BottomRight = new Point { X = 200, Y = 200 }
            };


            Console.ReadLine();
        }
    }
}
