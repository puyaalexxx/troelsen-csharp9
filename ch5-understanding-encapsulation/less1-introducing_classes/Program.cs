﻿using System;

namespace less1_introducing_classes
{
    class Program
    {
        static void Main(string[] args)
        {
            Car myCar = new Car();

            Console.WriteLine("***** Fun with Class Types *****\n");
            // Make a Car called Chuck going 10 MPH.
            Car chuck = new Car();
            chuck.PrintState();
            // Make a Car called Mary going 0 MPH.
            Car mary = new Car("Mary");
            mary.PrintState();
            // Make a Car called Daisy going 75 MPH.
            Car daisy = new Car("Daisy", 75);
            daisy.PrintState();

            myCar.petName = "Henry";
            myCar.currSpeed = 10;

            // Speed up the car a few times and print out the
            // new state.
            for (int i = 0; i <= 10; i++)
            {
                myCar.SpeedUp(5);
                myCar.PrintState();
            }
            Console.ReadLine();
        }
    }
}
