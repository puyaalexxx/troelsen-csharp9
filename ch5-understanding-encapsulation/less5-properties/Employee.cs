﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//better to use properties accross the site to avoid duplication
namespace less5_properties
{
    class Employee
    {
        // Field data.
        private string _empName;
        private int _empId;
        private float _currPay;
        private int _empAge;
        private string _empSSN;

        private EmployeePayTypeEnum _payType;
        public EmployeePayTypeEnum PayType
        {
            get => _payType;
            set => _payType = value;
        }

        // A static point of data.
        private static double _currInterestRate = 0.04;
        // A static property.
        public static double InterestRate
        {
            get { return _currInterestRate; }
            set { _currInterestRate = value; }
        }

        //read only property
        public string SocialSecurityNumberCHapter
        {
            get { return _empSSN; }
        }
        //or
        //public string SocialSecurityNumber => _empSSN;

        //write-only property
        public int Id
        {
            set { _empId = value; }
        }

        //private setter
        public int PrivatePropertie
        {
            get => _empAge;
            private set => _empAge = value;
        }

        //C# 7 - expression bodied members
        public int Age
        {
            get => _empAge;
            set => _empAge = value;
        }

        // Properties!
        public string Name
        {
            get { return _empName; }
            set
            {
                if (value.Length > 15)
                {
                    Console.WriteLine("Error! Name length exceeds 15 characters!");
                }
                else
                {
                    _empName = value;
                }
            }
        }
        // We could add additional business rules to the sets of these properties;
        // however, there is no need to do so for this example.

        public float Pay
        {
            get { return _currPay; }
            set { _currPay = value; }
        }

        // Updated constructors.
        public Employee() { }
        public Employee(string name, int id, float pay)
        : this(name, 0, id, pay, EmployeePayTypeEnum.Salaried) { }
        public Employee(string name, int age, int id, float pay, EmployeePayTypeEnum payType)
        {
            Name = name;
            Age = age;
            Id = id;
            Pay = pay;
            PayType = payType;
        }

        //using pattern matching with properties
        public void GiveBonus(float amount)
        {
            Pay = this switch
            {
                { PayType: EmployeePayTypeEnum.Commission }
                => Pay += .10F * amount,
                { PayType: EmployeePayTypeEnum.Hourly }
                => Pay += 40F * amount / 2080F,
                { PayType: EmployeePayTypeEnum.Salaried }
                => Pay += amount,
                _ => Pay += 0
            };
        }

        // Updated DisplayStats() method now accounts for age.
        public void DisplayStats()
        {
            Console.WriteLine("Name: {0}", Name);
            Console.WriteLine("Age: {0}", Age);
            Console.WriteLine("Pay: {0}", Pay);
        }
    }

    public enum EmployeePayTypeEnum
    {
        Hourly,
        Salaried,
        Commission
    }
}
