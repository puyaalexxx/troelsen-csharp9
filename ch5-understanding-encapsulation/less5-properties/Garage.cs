﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace less5_properties
{
    class Garage
    {

        //C# 6 - initializing automatic properties
        public int NumberOfCars1 { get; set; } = 1;
        // The hidden backing field is set to a new Car object.
        public Car MyAuto1 { get; set; } = new Car();

        // The hidden int backing field is set to zero!
        public int NumberOfCars { get; set; }
        // The hidden Car backing field is set to null!
        public Car MyAuto { get; set; }

        // Must use constructors to override default
        // values assigned to hidden backing fields.
        public Garage()
        {
            MyAuto = new Car();
            NumberOfCars = 1;
        }
        public Garage(Car car, int number)
        {
            MyAuto = car;
            NumberOfCars = number;
        }
    }
}
