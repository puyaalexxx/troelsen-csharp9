﻿using System;

namespace less5_properties
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Encapsulation *****\n");
            Employee emp = new Employee();

            // Reset and then get the Name property.
            emp.Name = "Marv";
            Console.WriteLine("Employee is named: {0}", emp.Name);

            // Print the current interest rate via property.
            Console.WriteLine("Interest Rate is: {0}", Employee.InterestRate);

            //testing pattern matching in properties
            Employee emp1 = new Employee("Marvin", 45, 123, 1000, EmployeePayTypeEnum.Salaried);
            Console.WriteLine(emp1.Pay);
            emp1.GiveBonus(100);
            Console.WriteLine(emp1.Pay);

            Console.ReadLine();
        }
    }
}
