﻿using System;

namespace less9_records
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fun with Records!");
            //Use object initialization
            Car myCar = new Car
            {
                Make = "Honda",
                Model = "Pilot",
                Color = "Blue"
            };
            Console.WriteLine("My car: ");
            DisplayCarStats(myCar);
            Console.WriteLine();
            //Use the custom constructor
            Car anotherMyCar = new Car("Honda", "Pilot", "Blue");
            Console.WriteLine("Another variable for my car: ");
            DisplayCarStats(anotherMyCar);
            Console.WriteLine();
            //Compile error if property is changed
            //myCar.Color = "Red";
            
            static void DisplayCarStats(Car c)
            {
                Console.WriteLine("Car Make: {0}", c.Make);
                Console.WriteLine("Car Model: {0}", c.Model);
                Console.WriteLine("Car Color: {0}", c.Color);
            }



            Console.WriteLine("/*************** RECORDS *********************/");


            //Use object initialization
            CarRecord1 myCarRecord = new CarRecord1
            {
                Make = "Honda",
                Model = "Pilot",
                Color = "Blue"
            };
            Console.WriteLine("My car: ");
            DisplayCarRecordStats(myCarRecord);
            Console.WriteLine();
            //Use the custom constructor
            CarRecord anotherMyCarRecord = new CarRecord("Honda", "Pilot", "Blue");
            Console.WriteLine("Another variable for my car: ");
            Console.WriteLine(anotherMyCarRecord.ToString());
            Console.WriteLine();
            //Compile error if property is changed
            //myCarRecord.Color = "Red";

            static void DisplayCarRecordStats(CarRecord1 c)
            {
                Console.WriteLine("Car Make: {0}", c.Make);
                Console.WriteLine("Car Model: {0}", c.Model);
                Console.WriteLine("Car Color: {0}", c.Color);
            }
            Console.WriteLine();

            ////////////////////////records equality

            //can't use object initialization with compact form of record types
            CarRecord myCar1 = new CarRecord("Honda", "Pilot", "Blue");
            CarRecord anotherMyCarRecord1 = new CarRecord("Honda", "Pilot", "Blue");

            // true - records are also reference types but behaves as value types
            Console.WriteLine($"Cars are the same? {myCar1.Equals(anotherMyCarRecord1)}"); 
            Console.WriteLine($"Cars are the same reference? {ReferenceEquals(myCar1, anotherMyCarRecord1)}");
            Console.WriteLine($"CarRecords are the same? {myCar1 == anotherMyCarRecord1}");
            Console.WriteLine($"CarRecords are not the same? {myCar1 != anotherMyCarRecord1}");
            Console.WriteLine();

            //////////////////////copying trecord behaves like classes though
            CarRecord carRecordCopy = anotherMyCarRecord;
            Console.WriteLine("Car Record copy results");
            Console.WriteLine($"CarRecords are the same? {carRecordCopy.Equals(anotherMyCarRecord)}");
            Console.WriteLine($"CarRecords are the same? {ReferenceEquals(carRecordCopy, anotherMyCarRecord)}");
            Console.WriteLine();

            //////////////////////C# 9 with construct for copying records
            CarRecord1 ourOtherCar = myCarRecord with { Model = "Odyssey" };
            Console.WriteLine("My copied car:");
            Console.WriteLine(ourOtherCar.ToString());
            Console.WriteLine("Car Record copy using with expression results");
            Console.WriteLine($"CarRecords are the same? {ourOtherCar.Equals(myCarRecord)}");
            Console.WriteLine($"CarRecords are the same? {ReferenceEquals(ourOtherCar, myCarRecord)}");
        }
    }
}
