﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace less9_records
{
    record CarRecord(string Make, string Model, string Color);

    //Referred to as a positional record type, the constructor defines the properties on the record, and all of the
    //other plumbing code has been removed.There are three considerations when using this syntax: the first is that
    //you cannot use object initialization of record types using the compact definition syntax, the second is that the
    //record must be constructed with the properties in the correct position, and the third is that the casing of the
    //properties in the constructor is directly translated to the casing of the properties on the record type.

    record CarRecord1
    {
        public string Make { get; init; }
        public string Model { get; init; }
        public string Color { get; init; }
        public CarRecord1() { }
        public CarRecord1(string make, string model, string color)
        {
            Make = make;
            Model = model;
            Color = color;
        }
    }
}
