﻿using System;

namespace less3_static_keyword
{
    class Program
    {
        static void Main(string[] args)
        {
            // Make an account.
            SavingsAccount s1 = new SavingsAccount(50);
            // Print the current interest rate.
            Console.WriteLine("Interest Rate is: {0}", SavingsAccount.GetInterestRate());
            // Try to change the interest rate via property.
            SavingsAccount.SetInterestRate(0.08);
            // Make a second account.
            SavingsAccount s2 = new SavingsAccount(100);
            // Should print 0.08...right??
            Console.WriteLine("Interest Rate is: {0}", SavingsAccount.GetInterestRate());

            //using ctatic class
            TimeUtilClass.PrintDate();
            TimeUtilClass.PrintTime();

            Console.ReadLine();
        }
    }
}
