﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace less3_static_keyword
{
    class SavingsAccount
    {
        public static double currInterestRate = 0.04;

        // Instance-level data.
        public double currBalance;
        public SavingsAccount(double balance)
        {
           // currInterestRate = 0.04; // This is static data!
            currBalance = balance;
        }

        // A static constructor!
        static SavingsAccount()
        {
            Console.WriteLine("In static ctor!");
            currInterestRate = 0.04;
        }

        // Static members to get/set interest rate.
        public static void SetInterestRate(double newRate)
        => currInterestRate = newRate;
        public static double GetInterestRate()
        => currInterestRate;
    }
}
