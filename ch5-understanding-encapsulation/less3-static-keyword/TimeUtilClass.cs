﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.Console;
using static System.DateTime;

namespace less3_static_keyword
{
    static class TimeUtilClass
    {
        public static void PrintTime() => Console.WriteLine(Now.ToShortTimeString());
        public static void PrintDate()
        => Console.WriteLine(Today.ToShortDateString());
    }
}
