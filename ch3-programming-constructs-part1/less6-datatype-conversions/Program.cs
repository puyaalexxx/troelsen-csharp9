﻿using System;

Console.WriteLine("***** Fun with type conversions *****");
// Add two shorts and print the result.
short numb1 = 9, numb2 = 10;
Console.WriteLine("{0} + {1} = {2}", numb1, numb2, Add(numb1, numb2));
Console.ReadLine();

static int Add(int x, int y)
{
    return x + y;
}

//explicit cast
static void NarrowingAttempt()
{
    byte myByte = 0;
    int myInt = 200;
    // Explicitly cast the int into a byte (no loss of data).
    myByte = (byte)myInt;
    Console.WriteLine("Value of myByte: {0}", myByte);
}

static void CheckedMethod()
{
    byte b1 = 100;
    byte b2 = 250;
    // This time, tell the compiler to add CIL code // to throw an exception if overflow/underflow // takes place.
    try
    {
        checked {
            byte sum = (byte)Add(b1, b2);
            Console.WriteLine("sum = {0}", sum);
        }
    }
    catch (OverflowException ex)
    {
        Console.WriteLine(ex.Message);
    }
}

static void UnCheckedMethod()
{
    byte b1 = 100;
    byte b2 = 250;
    // This time, tell the compiler to add CIL code // to not throw an exception if overflow/underflow // takes place.
    //you need to set checked errors 
    try
    {
        unchecked
        {
            byte sum = (byte)(b1 + b2);
            Console.WriteLine("sum = {0} ", sum);
        }
    }
    catch (OverflowException ex)
    {
        Console.WriteLine(ex.Message);
    }
}