﻿using System;

Console.WriteLine("Basic console IO");

GetUserData();

Console.ReadLine();

static void GetUserData()
{
    Console.WriteLine("Your name: ");
    string usernName = Console.ReadLine();
    Console.WriteLine("Your age: ");
    string userAge = Console.ReadLine();

    Console.WriteLine($"Hello {usernName}! You are {userAge} years old");
}