﻿using System;

ShowEnvirontmentDetails();

static void ShowEnvirontmentDetails()
{
    foreach (var drive in Environment.GetLogicalDrives())
    {
        Console.WriteLine($"Drive: {drive}");
        
    }

    Console.WriteLine($"Os: {Environment.OSVersion}");
    Console.WriteLine($"Number of processors: {Environment.ProcessorCount}");
    Console.WriteLine($"Net core version: {Environment.Version}");
}