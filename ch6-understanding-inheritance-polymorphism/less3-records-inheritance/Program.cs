﻿using System;

namespace less3_records_inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Record type inheritance!");
            Car c = new Car("Honda", "Pilot", "Blue");
            Minivan m = new Minivan("Honda", "Pilot", "Blue", 10);
            Console.WriteLine($"Checking MiniVan is-a Car:{m is Car}");
        }
    }
}
