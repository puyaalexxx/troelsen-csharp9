﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace less3_records_inheritance
{
    sealed record Minivan : Car
    {
        public int Seating { get; init; }
        public Minivan(string make, string model, string color, int seating) 
            : base(make,  model, color)
        {
            Seating = seating;
        }
    }
}
