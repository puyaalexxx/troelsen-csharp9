﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace less1_inheritance
{
    class Minivan : Car
    {
        public void TestMethod()
        {

            // OK! Can access public members
            // of a parent within a derived type.
            Speed = 10;
            // Error! Cannot access private
            // members of parent within a derived type.
            //_currSpeed = 10;
        }
    }
}
