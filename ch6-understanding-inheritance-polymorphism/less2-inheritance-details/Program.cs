﻿using System;

namespace less2_inheritance_details
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create a subclass object and access base class functionality.
            Console.WriteLine("***** The Employee Class Hierarchy *****\n");
            SalesPerson fred = new SalesPerson
            {
                Age = 31,
                Name = "Fred",
                SalesNumber = 50
            };

            Console.WriteLine("composition in action ");

            Manager chucky = new Manager("Chucky", 50, 92, 100000, 9000);
            double cost = chucky.GetBenefitCost();
            Console.WriteLine($"Benefit Cost: {cost}");

            Console.WriteLine("***** virtual methods *****\n");
            // A better bonus system!
            Manager chucky1 = new Manager("Chucky", 50, 92, 100000, 9000);
            chucky1.GiveBonus(300);
            chucky1.DisplayStats();
            Console.WriteLine();
            SalesPerson fran = new SalesPerson("Fran", 43, 93, 3000, 31);
            fran.GiveBonus(200);
            fran.DisplayStats();


            Console.ReadLine();
        }
    }
}
