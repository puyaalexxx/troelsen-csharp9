﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace less5_abstraction
{
    abstract class Shape
    {
        protected Shape(string name = "NoName")
        { PetName = name; }
        public string PetName { get; set; }
        // A single virtual method.
        public abstract void Draw();
    }
}
