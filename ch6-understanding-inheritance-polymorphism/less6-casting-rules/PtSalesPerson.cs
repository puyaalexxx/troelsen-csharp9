﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace less2_inheritance_details
{
    sealed class PtSalesPerson : SalesPerson
    {
        public PtSalesPerson(string fullName, int age, int empId, float currPay,  int numbOfSales)
        : base(fullName, age, empId, currPay, numbOfSales)
        { }
        // Assume other members here...
    }
}
