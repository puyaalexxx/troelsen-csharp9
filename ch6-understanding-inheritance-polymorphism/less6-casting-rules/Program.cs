﻿using less2_inheritance_details;
using System;



static void CastingExamples()
{
    // A Manager "is-a" System.Object, so we can
    // store a Manager reference in an object variable just fine.
    object frank = new Manager("Frank Zappa", 9, 3000, 40000, 5);
    // Error!
    //GivePromotion(frank);
    // OK!
    GivePromotion((Manager)frank);

    // A Manager "is-an" Employee too.
    Employee moonUnit = new Manager("MoonUnit Zappa", 2, 3001, 20000, 1);
    GivePromotion(moonUnit);


    // A PtSalesPerson "is-a" SalesPerson.
    SalesPerson jill = new PtSalesPerson("Jill", 834, 3002, 100000, 90);
    GivePromotion(jill);

    // Use "as" to test compatibility.
    object[] things = new object[4];
    things[0] = new Employee();
    things[1] = false;
    things[2] = new Manager("MoonUnit Zappa", 2, 3001, 20000, 1);
    things[3] = "Last thing";
    foreach (object item in things)
    {
        Employee h = item as Employee;
        if (h == null)
        {
            Console.WriteLine("Item is not a hexagon");
        }
        else
        {
            h.DisplayStats();
        }
    }

}

//the is keyword
static void GivePromotion(Employee emp)
{
    Console.WriteLine("{0} was promoted!", emp.Name);
    if (emp is SalesPerson)
    {
        Console.WriteLine("{0} made {1} sale(s)!", emp.Name,
        ((SalesPerson)emp).SalesNumber);
        Console.WriteLine();
    }
    else if (emp is Manager)
    {
        Console.WriteLine("{0} had {1} stock options...", emp.Name,
        ((Manager)emp).StockOptions);
        Console.WriteLine();
    }
}

//the is keyword C# 7
static void GivePromotion1(Employee emp)
{
    Console.WriteLine("{0} was promoted!", emp.Name);
    //Check if is SalesPerson, assign to variable s
    if (emp is SalesPerson s)
    {
        Console.WriteLine("{0} made {1} sale(s)!", s.Name,
        s.SalesNumber);
        Console.WriteLine();
    }
    //Check if is Manager, if it is, assign to variable m
    else if (emp is Manager m)
    {
        Console.WriteLine("{0} had {1} stock options...",
        m.Name, m.StockOptions);
        Console.WriteLine();
    }
    else if (emp is var _)
    {
        Console.WriteLine("Unable to promote {0}. Wrong employee type", emp.Name);
        Console.WriteLine();
    }

    //C# 9 pattern matching
    if (emp is not Manager and not SalesPerson)
    {
        Console.WriteLine("Unable to promote {0}. Wrong employee type", emp.Name);
        Console.WriteLine();
    }
}

//using a pattern matching
static void GivePromotion2(Employee emp)
{
    Console.WriteLine("{0} was promoted!", emp.Name);
    switch (emp)
    {
        case SalesPerson s:
            Console.WriteLine("{0} made {1} sale(s)!", emp.Name,
            s.SalesNumber);
            break;
        case Manager m:
            Console.WriteLine("{0} had {1} stock options...",
            emp.Name, m.StockOptions);
            break;
        case Employee _:
            Console.WriteLine("Unable to promote {0}. Wrong employee type", emp.Name);
            break;
    }
    Console.WriteLine();
}